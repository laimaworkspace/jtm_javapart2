package jtm.activity06;

public class Human implements Humanoid {
	int stomach;
	int birthWeight;
	int tmp;
	boolean isAlive;

	public Human() {
		stomach = 0;
		birthWeight = BirthWeight;
		isAlive = true;
	}

	@Override
	public void eat(Integer food) {
		stomach += food;

	}

	@Override
	public Integer vomit() {
		int food = stomach;
		stomach = 0;
		return food;
	}

	@Override
	public String isAlive() {
		if (isAlive)
			return "Alive";
		else
			return "Dead";

	}

	@Override
	public String killHimself() {
		isAlive = false;
		return isAlive();
	}

	@Override
	public int getWeight() {

		return BirthWeight + stomach;

	}

	@Override
	public String toString() {
		return "Human: " + getWeight() + " [" + stomach + "]";
	}

}
