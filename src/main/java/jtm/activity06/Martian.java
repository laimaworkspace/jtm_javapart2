package jtm.activity06;

public class Martian implements Humanoid, Alien, Cloneable {
	int weight;
	Object stomach;

	public Martian() {
		weight = -1;

	}

	@Override
	public void eat(Object item) {
		stomach = item;

		if (item instanceof Humanoid) {
			Humanoid tmp = (Humanoid) item;
			weight = weight + tmp.getWeight();

		}
		if (item instanceof Integer) {
			Integer tmp = (Integer) item;
			weight = weight + tmp;
		}
	}

	@Override
	public Object vomit() {

		return null;
	}

	@Override
	public int getWeight() {

		return weight;
	}

	@Override
	public String isAlive() {

		return "I AM IMMORTAL!";
	}

	@Override
	public String killHimself() {

		return isAlive();
	}

	@Override
	public void eat(Integer food) {
		eat((Object) food);
	}

	/*
	 * Implementation of Object clone() method for Cloneable interface
	 * 
	 * @see https://docs.oracle.com/javase/7/docs/api/java/lang/Cloneable.html
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return clone(this);
	}

	private Object clone(Object current) {
		if (current instanceof Integer)
			return (Integer) current;
		if (current instanceof Human) {
			Human tmpHuman = (Human) current;
			Human newHuman = new Human();
			newHuman.eat((Integer) tmpHuman.vomit());
			return newHuman;

		}
		if (current instanceof Martian) {
			Martian tmpMartian = (Martian) current;
			Martian newMartian = new Martian();
			newMartian.eat(tmpMartian.vomit());
			return newMartian;

		}

		return current;
		// TODO implement cloning of current object
		// and its stomach

	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ": " + getWeight() + " [" + stomach + "]";
	}

	public static void main(String[] args) throws CloneNotSupportedException {
		Martian m1 = new Martian();
		Martian m2 = new Martian();
		Martian m3 = new Martian();
		Martian m4 = new Martian();
		Martian m5 = new Martian();
		Martian m6 = new Martian();
		Martian m7 = new Martian();

		Human h1 = new Human();

		// m1.eat(100);
		m1.eat(m2);
		m2.eat(m3);
		m3.eat(m4);
		m4.eat(m5);
		m5.eat(m6);
		m6.eat(m7);
		m6.eat(h1);
		h1.eat(0);

		System.out.println(h1);
		System.out.println(m1);

	}
}
