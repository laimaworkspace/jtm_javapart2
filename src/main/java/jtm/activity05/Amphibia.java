package jtm.activity05;

import jtm.activity04.Road;

public class Amphibia extends jtm.activity04.Transport {

	private byte sails;
	private int wheels;

	public Amphibia(String id, float consumption, int tankSize, byte sails, int wheels) {
		super(id, consumption, tankSize);
		this.sails = sails;
		this.wheels = wheels;

	}

	@Override
	public String move(Road road) {

		if (road.getClass() == Road.class) {
			if (this.canMove(road)) {
				float neededFuel = road.getDistance() * (this.getConsumption() / 100);
				float fuelInTank = this.getFuelInTank() - neededFuel;
				this.setFuelInTank(fuelInTank);
				return this.getType() + " is driving on " + road + " with " + wheels + " wheels";
			} else
				return this.move(road);
		} else
			return this.getType() + " is sailing on " + road + " with " + this.sails + " sails";

	}
}
