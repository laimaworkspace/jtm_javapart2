package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Ship extends Transport {

	protected byte sails;

	public Ship(String id, byte sails) {
		super(id, 0, 0);
		this.sails = sails;

	}

	@Override
	public String move(Road road) {
		if (road instanceof WaterRoad)
			return super.getType() + " is sailing on " + road + " with " + this.sails + " sails";
		else
			return "Cannot sail on " + road;

	}

	public static void main(String[] args) {
		Ship ship = new Ship("aa", (byte) 5);
		System.out.println(ship.move(new WaterRoad("from", "to", 10)));
	}

}
