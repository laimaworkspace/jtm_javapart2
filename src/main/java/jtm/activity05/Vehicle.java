package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Vehicle extends Transport {
	protected int wheels;

	public Vehicle(String id, float consumption, int tankSize, int wheels) {
		super(id, consumption, tankSize);
		this.wheels = wheels;

	}

	@Override
	public String move(Road road) {
		if (road.getClass() == Road.class) {
			if (this.canMove(road)) {
				float neededFuel = road.getDistance() * (this.getConsumption() / 100);
				float fuelInTank = this.getFuelInTank() - neededFuel;
				this.setFuelInTank(fuelInTank);
				return this.getType() + " is driving on " + road + " with " + wheels + " wheels";
			} else
				return this.move(road);
		}
		return "Cannot drive on " + road;
	}
}
