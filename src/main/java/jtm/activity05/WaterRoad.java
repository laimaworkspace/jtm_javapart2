package jtm.activity05;

import jtm.activity04.Road;

public class WaterRoad extends Road {

	public WaterRoad(String from, String to, int distance) {
		super(from, to, distance);

	}

	public WaterRoad() {
		super();
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " " + super.toString();

	}

	public static void main(String[] args) {
		WaterRoad water = new WaterRoad("from", "to", 10);
		System.out.println(water);

	}

}
